const fs = require("fs")
const { getTodosLivros, modificarLivro} = require("../servicos/livro");
const  {getLivroPorId,deleteLivroPorId} = require("../servicos/livro")

function getLivros(req,res) {
    try {
        const livros = getTodosLivros();
        res.send(livros);
    } catch (error) {
        res.status(500);
        res.send(error.message);
    }

};

function getLivro(req,res) {
    try {
        const id = req.params.id;
        const livro = getLivroPorId(id);
        res.send(livro); 
    } catch (error) {
        res.status(500);
        res.send(error.message);
    }

    
};

function postLivro(req, res) {
    try {
        const livroNovo = req.body;
        insereLivro(livroNovo);
        res.status(201);
        res.send("Livro Inserido com sucesso");
    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
};

function insereLivro(livroNovo) {
    let livros = JSON.parse(fs.readFileSync("livros.json"));
    const novaListaDeLivros = [...livros, livroNovo]
    fs.writeFileSync("livros.json", JSON.stringify(novaListaDeLivros))
};

function pathLivro(req,res) {
    try {
        const id = req.params.id;
        const body = req.body
        modificarLivro(body, id)
        res.send("ITEM MODIFICADO!")
    } catch (error) {
        res.status(500);
        res.send(error.message)
    }
}

function deleteLivro(req, res) {
    try {
        const id = req.params.id;
        deleteLivroPorId(id)
        res.send("livro deletado com sucesso")
    } catch (error) {
        res.status(500)
        res.send(error.message)
    }
}

module.exports = {
    getLivros,
    getLivro,
    postLivro,
    pathLivro,
    deleteLivro
};