const fs = require("fs")

function getTodosLivros(req, res) {
     return JSON.parse(fs.readFileSync("livros.json"))
}



function getLivroPorId(id) {
     const livros = JSON.parse(fs.readFileSync("livros.json"));
     const livroFiltrado = livros.filter( livro => livro.id === id)[0];
     return livroFiltrado
}

function modificarLivro(modificacao, id) {
     let livrosAtuais = JSON.parse(fs.readFileSync("livros.json"));
     const indiceModificado = livrosAtuais.findIndex(livro => livro.id === id);
     const conteudoAlterado = {...livrosAtuais[indiceModificado], ...modificacao}
     livrosAtuais[indiceModificado] = conteudoAlterado;
     fs.writeFileSync("livros.json", JSON.stringify(livrosAtuais));
}

function deleteLivroPorId(id) {
     const livros = JSON.parse(fs.readFileSync("livros.json"))
     const livroFiltrados = livros.filter(livro => livro.id !== id )
     fs.writeFileSync("livros.json", JSON.stringify(livroFiltrados))
}



module.exports = {
    getTodosLivros,
    getLivroPorId,
    modificarLivro,
    deleteLivroPorId
}