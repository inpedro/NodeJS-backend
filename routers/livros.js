const { Router } = require("express");
const {getLivros, getLivro, postLivro, insereLivro, pathLivro, deleteLivro} = require("../controller/livros")
const router = Router();


//ROTAS
router.get('/', getLivros);
router.get('/:id', getLivro);



//TIPOS DE SOLCITACOES RTS
router.post('/', postLivro);

router.patch('/:id', pathLivro);

router.delete('/:id', deleteLivro );

module.exports = router;