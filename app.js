const rtLivro = require("./routers/livros")
const express = require("express")

const app = express()
app.use(express.json())
app.use('/livros', rtLivro)
const port = 8000

// app.get chama requisição para o banco
app.get('/',(req, res) => {
    res.send("Diga Oi - laagum")
})

// app.listen mostra status para porta de requisão 8000(back-end)
app.listen(port, () => {
    console.log(`Porta ativa ${port}`)
})
